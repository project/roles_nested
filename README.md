# Roles Nested

There are note parents and child only roles draggable arrange.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/roles_nested).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/roles_nested).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Goto: /admin/people/roles-nested.


## Maintainers

- Deepak Bhati - [heni_deepak](https://www.drupal.org/u/heni_deepak)
- Radheshyam Kumawat - [radheymkumar](https://www.drupal.org/u/radheymkumar)